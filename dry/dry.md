# Dry

## א

### 1

| $k$  |       $\#possiblePaths$        | $\log_2(\#possiblePaths)$ |   Calculation time   |
| :--: | :----------------------------: | :-----------------------: | :------------------: |
|  10  |         10! = 3628800          |          21.791           |       < 1 sec        |
|  13  |        13! = 6227020800        |          32.536           |      5.799 sec       |
|  15  |      15! = 1307674368000       |          40.250           |     20.298 mins      |
|  16  |      16! = 20922789888000      |          44.250           |     5.413 hours      |
|  17  |     17! = 355687428096000      |          48.338           |      3.834 days      |
|  20  |   20! = 2432902008176640000    |          61.077           |     71.849 years     |
|  21  |   21! = 51090942171709440000   |          65.470           |    1508.820 years    |
|  24  | 24! = 620448401733239439360000 |          79.038           | 18.323 million years |

## ג

### 2

* Minimal is 0. For example, we've reached final state (went trough all apartments and put all the tests to labs) and after this we visited every laboratory (that hasn't been visited). For such configuration there is no operator to apply.
* Maximal is k+m (from the initial state). It means we can visit any apartment (to make tests) or any lab (to take matoshim). 

### 3

Circles are not possible in defined state space. Obviously, we cannot visit the same apartment twice. If we visit the lab we've visited before, $VisitedLabs$ will be different, so it will be two different states. In all other options we looking at two different location, so it's obvious that it will be two different states.

### 4

There is an infinite number of configurations since number of matoshim for configuration is defined to be any natural number (and there is an infinity of natural numbers). And, of course, not all configurations are achievable. For example, if summary number of matoshim in all labs is $g$, then the maximum number of matoshim for reachable configuration will be $InitialNrMatoshimAmb+g$, and there are configurations with number of matoshim which is greater than this value (since number of matoshim can be any natural number).

### 5

It is possible. For example, a problem was given with labs set being an empty set (no labs), then we firstly move to an apartment with number of people equals to initial number of matoshim at the ambulance. From this configuration there is no way to apply an operator since there are no more matoshim (cannot move to another apartment) and there are no laboratories (cannot move to laboratory). Another example can be a problem where there is only one lab with very small number of matoshim ($=m$), such that $InitialNrMatoshimAmb+m$ is less than number of matoshim need for any of the apartments. Firstly we move to the single lab, and then there is no operator to apply.

### 6

Minimal number of edges is k+1 (since we need to go through every apartment anyway and put the tests to laboratory) in case we have enough matoshim initially to make a drive without visiting laboratories. Maximal number of edges is 2(k-1)+(m-1)+(1+1)=2k+m-1. Explanation:

* 2(k-1) - go through k-1 apartments putting tests to the same laboratory after each apartment.
* (m-1) - go through m-1 laboratories left (take matoshim).
* (1+1) - go trough the last apartment and put tests to an arbitrary laboratory.

### 7

$$
Succ(s)=\{(d.loc,s.Taken\cup\{d\},s.Transferred,\\s.Matoshim-d.roommates,s.VisitedLabs)\mid CanVisit(s,d)\}\cup\\\{(l.loc,\empty,s.Matoshim+l.Matoshim,s.VisitedLabs\cup\{l\})\mid CanVisit(s,l)\}
$$

## ד

### 8

#### g

```
StreetsMap(src: 54 dst: 549)          UniformCost                   time:   0.58   #dev: 17354   |space|: 17514    total_g_cost:  7465.52560   |path|: 136   path: [   54  ==>     55  ==>     56  ==>     57  ==>     58  ==>     59  ==>     60  ==>  28893  ==>  14580  ==>  14590  ==>  14591  ==>  14592  ==>  14593  ==>  81892  ==>  25814  ==>     81  ==>  26236  ==>  26234  ==>   1188  ==>  33068  ==>  33069  ==>  33070  ==>  15474  ==>  33071  ==>   5020  ==>  21699  ==>  33072  ==>  33073  ==>  33074  ==>  16203  ==>   9847  ==>   9848  ==>   9849  ==>   9850  ==>   9851  ==>    335  ==>   9852  ==>  82906  ==>  82907  ==>  82908  ==>  82909  ==>  95454  ==>  96539  ==>  72369  ==>  94627  ==>  38553  ==>  72367  ==>  29007  ==>  94632  ==>  96540  ==>   9269  ==>  82890  ==>  29049  ==>  29026  ==>  82682  ==>  71897  ==>  83380  ==>  96541  ==>  82904  ==>  96542  ==>  96543  ==>  96544  ==>  96545  ==>  96546  ==>  96547  ==>  82911  ==>  82928  ==>  24841  ==>  24842  ==>  24843  ==>   5215  ==>  24844  ==>   9274  ==>  24845  ==>  24846  ==>  24847  ==>  24848  ==>  24849  ==>  24850  ==>  24851  ==>  24852  ==>  24853  ==>  24854  ==>  24855  ==>  24856  ==>  24857  ==>  24858  ==>  24859  ==>  24860  ==>  24861  ==>  24862  ==>  24863  ==>  24864  ==>  24865  ==>  24866  ==>  82208  ==>  82209  ==>  82210  ==>  21518  ==>  21431  ==>  21432  ==>  21433  ==>  21434  ==>  21435  ==>  21436  ==>  21437  ==>  21438  ==>  21439  ==>  21440  ==>  21441  ==>  21442  ==>  21443  ==>  21444  ==>  21445  ==>  21446  ==>  21447  ==>  21448  ==>  21449  ==>  21450  ==>  21451  ==>    621  ==>  21452  ==>  21453  ==>  21454  ==>  21495  ==>  21496  ==>    539  ==>    540  ==>    541  ==>    542  ==>    543  ==>    544  ==>    545  ==>    546  ==>    547  ==>    548  ==>    549
```

## ה

### 11

```
StreetsMap(src: 54 dst: 549)          A* (h=AirDist, w=0.500)       time:   0.10   #dev: 2015    |space|: 2229     total_g_cost:  7465.52560   |path|: 136   path: [   54  ==>     55  ==>     56  ==>     57  ==>     58  ==>     59  ==>     60  ==>  28893  ==>  14580  ==>  14590  ==>  14591  ==>  14592  ==>  14593  ==>  81892  ==>  25814  ==>     81  ==>  26236  ==>  26234  ==>   1188  ==>  33068  ==>  33069  ==>  33070  ==>  15474  ==>  33071  ==>   5020  ==>  21699  ==>  33072  ==>  33073  ==>  33074  ==>  16203  ==>   9847  ==>   9848  ==>   9849  ==>   9850  ==>   9851  ==>    335  ==>   9852  ==>  82906  ==>  82907  ==>  82908  ==>  82909  ==>  95454  ==>  96539  ==>  72369  ==>  94627  ==>  38553  ==>  72367  ==>  29007  ==>  94632  ==>  96540  ==>   9269  ==>  82890  ==>  29049  ==>  29026  ==>  82682  ==>  71897  ==>  83380  ==>  96541  ==>  82904  ==>  96542  ==>  96543  ==>  96544  ==>  96545  ==>  96546  ==>  96547  ==>  82911  ==>  82928  ==>  24841  ==>  24842  ==>  24843  ==>   5215  ==>  24844  ==>   9274  ==>  24845  ==>  24846  ==>  24847  ==>  24848  ==>  24849  ==>  24850  ==>  24851  ==>  24852  ==>  24853  ==>  24854  ==>  24855  ==>  24856  ==>  24857  ==>  24858  ==>  24859  ==>  24860  ==>  24861  ==>  24862  ==>  24863  ==>  24864  ==>  24865  ==>  24866  ==>  82208  ==>  82209  ==>  82210  ==>  21518  ==>  21431  ==>  21432  ==>  21433  ==>  21434  ==>  21435  ==>  21436  ==>  21437  ==>  21438  ==>  21439  ==>  21440  ==>  21441  ==>  21442  ==>  21443  ==>  21444  ==>  21445  ==>  21446  ==>  21447  ==>  21448  ==>  21449  ==>  21450  ==>  21451  ==>    621  ==>  21452  ==>  21453  ==>  21454  ==>  21495  ==>  21496  ==>    539  ==>    540  ==>    541  ==>    542  ==>    543  ==>    544  ==>    545  ==>    546  ==>    547  ==>    548  ==>    549]
```

Relative saving here is:
$$
\frac{\#dev(UniformCost)-\#dev(A_{AirDist}^*)}{\#dev(UniformCost)}=\frac{17354-2015}{17354}\approx0.8839
$$

### 12

![q12plot](C:\Users\gsemy\hw\ai-hw1\dry\q12plot.svg)x

Of course, decision usually depends on various real-world limitations. On graph we can see the tendency described in class, but it is not strict. I would choose $\approx 0.53$ since we get best quality solution (for the current approach) with relatively low #Expanded states.

## ו

### 15

1. ```python
   @dataclass(frozen=True)
   ```
   
2. Because Python holds actually pointer to an object and not the copy of an object, it won't be enough to just make `MDAState` frozen. In case some class variables (attributes) are mutable, `MDAState` object can be changed. In code, all attributes of the `MDAState` are immutable (frozen):

   1. `current_site` is immutable since in definition of each class from `Union` there is a line `@dataclass(frozen=True)`.
   2. `tests_on_ambulance, tests_transferred_to_lab, visited_labs ` are from type `FrozenSet`.
   3. `nr_matoshim_on_ambulance` is `int` (atomic type).

3. if `tests_on_ambulance` and `visited_labs` are from the type `set` (hence mutable):

   ```python
   def expand_state_with_costs(self, state_to_expand: GraphProblemState) -> Iterator[OperatorResult]:
       assert isinstance(state_to_expand, MDAState)
       for apartment in self.get_reported_apartments_waiting_to_visit(state_to_expand):
           succ_state = state_to_expand
           if apartment in state_to_expand.tests_on_ambulance or apartment in state_to_expand.tests_transferred_to_lab:
               continue
           elif apartment.nr_roommates > state_to_expand.nr_matoshim_on_ambulance:
               continue
           elif (apartment.nr_roommates > self.problem_input.ambulance.taken_tests_storage_capacity - state_to_expand.get_total_nr_tests_taken_and_stored_on_ambulance()):
               continue
           else:
               # !!! This is critical line !!!
               succ_state.tests_on_ambulance.add(apartment)
               # !!! This is critical line !!!
               succ_state.nr_matoshim_on_ambulance - apartment.nr_roommates
               yield OperatorResult(succ_state, self.get_operator_cost(state_to_expand, succ_state))
       for lab in self.problem_input.laboratories:
           succ_state = state_to_expand
           if lab in state_to_expand.visited_labs and len(state_to_expand.tests_on_ambulance) == 0:
               continue
           else:
               succ_state.tests_on_ambulance = frozenset()
               succ_state.tests_transferred_to_lab.union(state_to_expand.tests_on_ambulance)
               if lab in state_to_expand.visited_labs:
                   continue
               else:
                   succ_state.nr_matoshim_on_ambulance += lab.max_nr_matoshim
               if not (lab in state_to_expand.visited_labs):
                   # !!! This is critical line !!! 
                   succ_state.visited_labs.add(lab)
                   # !!! This is critical line !!!
               else:
                   continue
               yield OperatorResult(succ_state, self.get_operator_cost(state_to_expand, succ_state))
   ```

The lines between `# !!! This is critical line !!!` are source of bug. Since `tests_on_ambulance` and `visited_labs` are mutable sets, we can just add new object to them, without need to create a new set, but since `state_to_expand` still pointing to the memory place where original state is stored, we will change it (that leads to bug). 

### 18

`MDAMaxAirDistHeuristic` is acceptable. Let's assume that two point which construct the maximum distance are x and y (from the points that are left to go over them), with the law of generality we can assume that x is the point we will reach before y, now let's look at the configuration right after we've reached x. From this point we will have to make a path at least as long as our heuristic (air distance between x and y), since we anyway need go over vertex y and `AirDist(x,y)` is shortest path between x and y in Euclidean space. That is why our heuristic is $\le h^*$, so it is acceptable.

### 21

`MDASumAirDistHeuristic` is not acceptable. Let's see next diagram:

```mermaid
graph LR
A((0,0)) --- B((2,0))
    B --- C((3,0))
    	C --- D((5,0))
```

Assume we start at (2,0) (coordinates on the map). Then our `MDASumAirDistHeuristic` will output 8 (from (2,0) to (3,0), then to (5,0) and finally to (0,0)), but $h^*(...)=7$ (from (2,0) to (0,0), then to (3,0) and finally to (5,0)).

### 24

`MDAMSTAirDistHeuristic` is acceptable. Imagine that set of apartments (vertices) left to go over them is S. Let G be a graph with edge (with weight of air distance between two vertices) between each two vertices. Now, let's see what an ideal solution from this configuration could be. First of all, the solution path must be a path, that goes over each vertex of G once. For example, the optimal path is the path that goes over vertices in G in order $x_0,x_1,...,x_n$, where $x_i\in S$. Then sum of the edges weights $\sum_{i=0}^{n-1}(x_i,x_{i+1}).weight=sumOfWeights$ is less or equal than $h^*(...)$, because air distance between vertices is the shortest distance in Euclidean space. And since straightforward path through $x_0,x_1,...,x_n$ is connected graph, $MST(G).weight\le sumOfWeights\le h^*(...)$, so `MDAMSTAirDistHeuristic` is acceptable.

### 25

`MDAMSTAirDistHeuristic` over `small_mda_problem_with_distance_cost`:

![q25plotMST](C:\Users\gsemy\hw\ai-hw1\dry\q25plotMST.svg)

Reasonable choice for graph above can be:

* 0.655 - optimal solution cost with least possible (for optimal solution cost) #expanded states.
* 0.692 - close to optimal solution with even smaller #expanded states.

`MDASumAirDistHeuristic` over `moderate_mda_problem_with_distance_cost`:

![q25plotSA](C:\Users\gsemy\hw\ai-hw1\dry\q25plotSA.svg)

Reasonable choice for graph above is 0.718 since then we are getting optimal solution cost with close to optimal #expanded states. We can still see the tendencies mentioned in question 12.

## ז

### 26

`MDAMaxAirDistHeuristic` is not acceptable under provided cost function. Let's see following diagram:

```mermaid
graph LR
	A(("Start")) -- 100 --- B((Apart)) -- 1 --- C((Lab))
```

Let's assume we currently at the `Start` vertex, and the is one apartment left (`Apart`), then heuristic function (`MDAMaxAirDistHeuristic` ) output will be 100, but under $cost_d^{test\,travel}$ the path `Start`->`Apart`->`Lab` gives us $100\cdot 0+1\cdot 1=1<100$.

### 27

`MDASumAirDistHeuristic` is not acceptable due to the same example as in question 26. `MDASumAirDistHeuristic` output will be 100 (edge from `Start` to `Apart`), but path `Start`->`Apart`->`Lab` gives us $100\cdot 0+1\cdot 1=1<100$.

### 28

`MDAMSTAirDistHeuristic` is not acceptable due to the same example as in question 26. MST in that case will be:

```mermaid
graph LR
	A(("Start")) -- 100 --- B((Apart))
```

It's weight is 100, but path `Start`->`Apart`->`Lab` gives us $100\cdot 0+1\cdot 1=1<100$.

### 30

1. Notice that during our run we will carry every existing test (needed to be done test) to some lab. From here we can say that the solution cost will be sum of distances every test went between the moment it was took and the moment it was passed to a lab. If we minimize each test distance (distance that test passed between the moment it was taken and the moment it was passed to a lab), we will minimize overall solution cost. According to this, `MDATestsTravelTimeToNearestLabHeuristic` output is the minimum solution cost (it indeed will be a solution cost if distances between each apartment to it's closest lab equal to air distances, in other words, there is a straight path between each apartment  and it's closest lab, and we don't encounter matoshim problems), so it is less than $h^*(...)$ output, so `MDATestsTravelTimeToNearestLabHeuristic` is acceptable.
2. From what have been said previous paragraph, `MDATestsTravelTimeToNearestLabHeuristic` will output minimum solution (the same as perfect heuristic) if if distances between each apartment to it's closest lab equal to air distances and we don't run into matoshim problems following such a path (even this might not be a problem, since we will gather needed matoshim riding without tests on ambulance dodging solution cost increment).

### 31

```
MDA(moderate_MDA(8):TestsTravelDistance)   A* (h=MDA-TimeObjectiveSumOfMinAirDistFromLab, w=0.500)   time:  34.64   #dev: 29180   |space|: 41799    total_g_cost: 104387.48471   total_cost: MDACost(dist= 137546.342m, tests-travel= 104387.485m)   |path|: 18
```

Let's see the example of one of the previous runs:

```
MDA(moderate_MDA(8):Distance)         A* (h=MDA-MST-AirDist, w=0.500)   time:  41.47   #dev: 29766   |space|: 40024    total_g_cost: 58254.18667   total_cost: MDACost(dist=  58254.187m, tests-travel= 131811.935m)   |path|: 13
```

We can see that tests-travel decreased (relative to the previous run) and dist increased (trade-off), it's indication that we indeed minimize relative measure.

## For q32-33 corrected algorithm version will be used

### 32

It's true. Since we can assume, that there is an finite number of vertices (states) in $\mathcal{P(\mathcal{S})}$, we can say that UCS is complete. Let's now write path of the original solution as $\langle s_0,s_1,...,s_t\rangle$ (where $s_i$ is some state from original states space). Since $\forall s_i$ in solution you can apply an operator to get an $s_{i+1}$, you can also apply operators from new search space on $\langle s_o,s_1,...,s_i\rangle$ (for example) to get $\langle s_0,s_1,...,s_i,s_{i+1}\rangle$, which means we can get to $\langle s_0,s_1,...,s_t\rangle\in G^P$ (goal state since $s_t\in G$), that is why algorithm will return a solution.

### 33

It's true. Since there are finite number of states in $\mathcal{P(\mathcal{S})}$, we can say that UCS is acceptable (meaning it will return the optimal solution). Optimal solution for given problem is the solution with $cost_{MDA}^{travel\,dist}$ for the solution path, while still satisfying $cost_{MDA}^{dist}$ for solution path is $\le (1+\epsilon)C_{dist}^*$, in other words optimal solution $\in OptimalPaths$. From thought above we can conclude that if solution is returned, it will fulfill combined criterion.

### 34

```
MDA(moderate_MDA(8):TestsTravelDistance)   A* (h=MDA-TimeObjectiveSumOfMinAirDistFromLab, w=0.500)   time:  30.05   #dev: 28811   |space|: 40722    total_g_cost: 104387.48471   total_cost: MDACost(dist=  89855.645m, tests-travel= 104387.485m)   |path|: 15    path: [(loc: initial-location tests on ambulance: [] tests transferred to lab: [] #matoshim: 3 visited labs: [])  ==(visit Raven Woolum)==>  (loc: test @ Raven Woolum tests on ambulance: ['Raven Woolum (2)'] tests transferred to lab: [] #matoshim: 1 visited labs: [])  ==(go to lab Bouldin-Boyland)==>  (loc: lab Bouldin-Boyland tests on ambulance: [] tests transferred to lab: ['Raven Woolum (2)'] #matoshim: 5 visited labs: ['Bouldin-Boyland'])  ==(visit Hana Hockman)==>  (loc: test @ Hana Hockman tests on ambulance: ['Hana Hockman (2)'] tests transferred to lab: ['Raven Woolum (2)'] #matoshim: 3 visited labs: ['Bouldin-Boyland'])  ==(go to lab Woolum-Mulholland)==>  (loc: lab Woolum-Mulholland tests on ambulance: [] tests transferred to lab: ['Raven Woolum (2)', 'Hana Hockman (2)'] #matoshim: 7 visited labs: ['Bouldin-Boyland', 'Woolum-Mulholland'])  ==(visit Gussie Foran)==>  (loc: test @ Gussie Foran tests on ambulance: ['Gussie Foran (2)'] tests transferred to lab: ['Raven Woolum (2)', 'Hana Hockman (2)'] #matoshim: 5 visited labs: ['Bouldin-Boyland', 'Woolum-Mulholland'])  ==(visit Veronique Katz)==>  (loc: test @ Veronique Katz tests on ambulance: ['Veronique Katz (1)', 'Gussie Foran (2)'] tests transferred to lab: ['Raven Woolum (2)', 'Hana Hockman (2)'] #matoshim: 4 visited labs: ['Bouldin-Boyland', 'Woolum-Mulholland'])  ==(go to lab Neri-Basta)==>  (loc: lab Neri-Basta tests on ambulance: [] tests transferred to lab: ['Veronique Katz (1)', 'Raven Woolum (2)', 'Hana Hockman (2)', 'Gussie Foran (2)'] #matoshim: 8 visited labs: ['Bouldin-Boyland', 'Neri-Basta', 'Woolum-Mulholland'])  ==(visit Kurt Dockstader)==>  (loc: test @ Kurt Dockstader tests on ambulance: ['Kurt Dockstader (4)'] tests transferred to lab: ['Veronique Katz (1)', 'Raven Woolum (2)', 'Hana Hockman (2)', 'Gussie Foran (2)'] #matoshim: 4 visited labs: ['Bouldin-Boyland', 'Neri-Basta', 'Woolum-Mulholland'])  ==(go to lab Neri-Basta)==>  (loc: lab Neri-Basta tests on ambulance: [] tests transferred to lab: ['Kurt Dockstader (4)', 'Veronique Katz (1)', 'Raven Woolum (2)', 'Hana Hockman (2)', 'Gussie Foran (2)'] #matoshim: 4 visited labs: ['Bouldin-Boyland', 'Neri-Basta', 'Woolum-Mulholland'])  ==(visit Pierre Lowman)==>  (loc: test @ Pierre Lowman tests on ambulance: ['Pierre Lowman (3)'] tests transferred to lab: ['Kurt Dockstader (4)', 'Veronique Katz (1)', 'Raven Woolum (2)', 'Hana Hockman (2)', 'Gussie Foran (2)'] #matoshim: 1 visited labs: ['Bouldin-Boyland', 'Neri-Basta', 'Woolum-Mulholland'])  ==(go to lab Lowman-Kohn)==>  (loc: lab Lowman-Kohn tests on ambulance: [] tests transferred to lab: ['Kurt Dockstader (4)', 'Pierre Lowman (3)', 'Veronique Katz (1)', 'Raven Woolum (2)', 'Hana Hockman (2)', 'Gussie Foran (2)'] #matoshim: 7 visited labs: ['Bouldin-Boyland', 'Neri-Basta', 'Woolum-Mulholland', 'Lowman-Kohn'])  ==(visit Krysta Valentine)==>  (loc: test @ Krysta Valentine tests on ambulance: ['Krysta Valentine (3)'] tests transferred to lab: ['Kurt Dockstader (4)', 'Pierre Lowman (3)', 'Veronique Katz (1)', 'Raven Woolum (2)', 'Hana Hockman (2)', 'Gussie Foran (2)'] #matoshim: 4 visited labs: ['Bouldin-Boyland', 'Neri-Basta', 'Woolum-Mulholland', 'Lowman-Kohn'])  ==(go to lab Woolum-Mulholland)==>  (loc: lab Woolum-Mulholland tests on ambulance: [] tests transferred to lab: ['Kurt Dockstader (4)', 'Pierre Lowman (3)', 'Veronique Katz (1)', 'Krysta Valentine (3)', 'Raven Woolum (2)', 'Hana Hockman (2)', 'Gussie Foran (2)'] #matoshim: 4 visited labs: ['Bouldin-Boyland', 'Neri-Basta', 'Woolum-Mulholland', 'Lowman-Kohn'])  ==(visit Cleora Alaniz)==>  (loc: test @ Cleora Alaniz tests on ambulance: ['Cleora Alaniz (4)'] tests transferred to lab: ['Kurt Dockstader (4)', 'Pierre Lowman (3)', 'Veronique Katz (1)', 'Krysta Valentine (3)', 'Raven Woolum (2)', 'Hana Hockman (2)', 'Gussie Foran (2)'] #matoshim: 0 visited labs: ['Bouldin-Boyland', 'Neri-Basta', 'Woolum-Mulholland', 'Lowman-Kohn'])  ==(go to lab Lowman-Kohn)==>  (loc: lab Lowman-Kohn tests on ambulance: [] tests transferred to lab: ['Cleora Alaniz (4)', 'Veronique Katz (1)', 'Gussie Foran (2)', 'Kurt Dockstader (4)', 'Pierre Lowman (3)', 'Krysta Valentine (3)', 'Raven Woolum (2)', 'Hana Hockman (2)'] #matoshim: 0 visited labs: ['Bouldin-Boyland', 'Neri-Basta', 'Woolum-Mulholland', 'Lowman-Kohn'])]
```

Comparison table (ordered by question numbers):

| Question | Optimization objective |   Heuristic    | dev   | space | dist        | tests-travel |
| -------- | ---------------------- | :------------: | ----- | ----- | ----------- | ------------ |
| 17       | dist                   |      MAD       | 33261 | 41773 | 58254.187m  | 131811.935m  |
| 20       | dist                   |      SAD       | 26966 | 38550 | 58254.187m  | 131811.935m  |
| 23       | dist                   |     MSTAD      | 29766 | 40024 | 58254.187m  | 131811.935m  |
| 31       | tests-travel           |    TTDTNLH     | 29180 | 41799 | 137546.342m | 104387.485m  |
| 34       | both                   | MSTAD & TTDTNL | 28811 | 40722 | 89855.645m  | 104387.485m  |

Heuristic acronyms:

* MAD = MaxAirDist
* SAD = SumAirDist
* MSTAD = MSTAirDist
* TTDTNL = TestsTravelDistToNearestLab

We can see that with $\mathcal{A}_2$ we got best tests-travel with best possible dist for optimal tests-travel. The value requested:
$$
\frac{C_{dist}^*}{DistCost(ReturnedSolution)}-1=\dfrac{58254.187}{89855.645}-1=0.6483086-1=-0.3516914
$$

### 35

It is not true (tested various $\epsilon$ values (doesn't work for very small values) in code), but haven't find example small enough to put here.

### 36

It's true. Since both $A^*$ runs are done with acceptable heuristics, we can say that they are acceptable ($A^*$ searches). From here, firstly we get an optimal $C_{dist}^*$, and then lowest total $cost_{MDA}^{test\,travel}$ which satisfies (path that solution is associated with, let's name it $p$) $p\in DistEpsOptimal$, which means $p\in OptimalPaths$ and from here fulfills combined criterion.

### 37

$\mathcal{A_2}$ is better than $\mathcal{A_1}$ in terms of running time because it doesn't insert some of the discovered vertices to `open`.

### 39

```
MDA(small_MDA(5):Distance)            A* (h=MDA-MST-AirDist, w=0.500)   time:  11.30   #dev: 575     |space|: 947      total_g_cost: 31528.65909   total_cost: MDACost(dist=  31528.659m, tests-travel=  52112.429m)   |path|: 8
MDA(small_MDA(5):Distance)            A*eps (h=MDA-MST-AirDist, w=0.500)   time:   0.89   #dev: 564     |space|: 933      total_g_cost: 31528.65909   total_cost: MDACost(dist=  31528.659m, tests-travel=  52112.429m)   |path|: 8
```

We saved 11 #devs and it was awaited, since actually, each time we take the vertex from open, we choose firstly from those who are appropriate by acceptable heuristic, and then make use of non-acceptable heuristic to introduce helping difference between vertices found appropriable.

### 41

```
MDA(moderate_MDA(8):Distance)         Anytime-A* (h=MDA-MST-AirDist, w=0.800)   time:  23.99   #dev: 1027    |space|: 740      total_g_cost: 64055.65000   total_cost: MDACost(dist=  64055.650m, tests-travel= 131870.337m)   |path|: 13
MDA(moderate_MDA(8):Distance)         A* (h=MDA-MST-AirDist, w=0.800)   time:   0.25   #dev: 114     |space|: 408      total_g_cost: 64055.65000   total_cost: MDACost(dist=  64055.650m, tests-travel= 131870.337m)   |path|: 13
```

Here we can see that for `Anytime-A*` we got less #dev, space and time (not absolute measure) for heuristic weight = 0.8.